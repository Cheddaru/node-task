// Build small REST API with Express

console.log("Server-side program starting...")

const express = require('express')
const app = express()
const PORT = 3000

app.get('/', function (req, res) {
  res.send('Hello World')
})

app.listen(PORT, () => console.log(
    `Server listening on http://localhost:${PORT}`
))

/**
 * This arrow function adds two numbers (a and b) together
 * @param {Number} a First parameter
 * @param {Number} b Becond parameter
 * @return {Number} Return the addition
 */
const add = (a, b) => {
    const sum = a + b
    return sum
}

// /add?a=value&b=value
app.get("/add", (req, res) => {
    console.log(req.query)
    const sum = add(parseInt(req.query.a), parseInt(req.query.b))
    res.send(sum.toString());
})