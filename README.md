# node-task

## **This repo is only for educational purposes only!**

This is a task for Ohjelmistojen ylläpito ja testaus

## node app

This node app currently has 2 endpoints

* `http://localhost:3000/`
  * Returns Hello World

* `http://localhost:3000/add?a=number&b=number`
  * Adds 2 numbers
  * Used with query parameters a and b